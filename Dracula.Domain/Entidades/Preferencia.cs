﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Dracula.Domain {
    public class Preferencia {

        public ObjectId _id { get; set; }

        public string Descricao { get; set; }
    }
}
