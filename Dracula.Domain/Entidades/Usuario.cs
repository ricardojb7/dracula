﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Dracula.Domain {
    public class Usuario {

        public ObjectId _id { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public int TipoUsuario { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Descricao { get; set; }
        public Foto FotoPerfil { get; set; }
        
        public List<Foto> ListaFotos { get; set; }
        public List<Usuario> ListaAmigos { get; set; }
        public List<Usuario> ListaSeguidores { get; set; }
        public List<Usuario> ListaSeguindo { get; set; }
        public List<Preferencia> ListaPreferencias { get; set; }
        public List<Evento> ListaEventos { get; set; }
        public List<Comanda> ListaComandas { get; set; }
        public List<FormaPagamento> ListaFormasPagamento { get; set; }
        public List<Publicacao> ListaPublicacoes { get; set; }
        
        public Usuario()
        {
            ListaAmigos = new List<Usuario>();
            ListaFotos = new List<Foto>();
            ListaSeguidores = new List<Usuario>();
            ListaSeguindo = new List<Usuario>();
            ListaPreferencias = new List<Preferencia>();
            ListaEventos = new List<Evento>();
            ListaComandas = new List<Comanda>();
            ListaFormasPagamento = new List<FormaPagamento>();
            ListaPublicacoes = new List<Publicacao>();

        }
    }
}
