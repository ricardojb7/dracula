﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dracula.Domain.Repositorio
{
    interface IUsuarioRepository
    {
        void AddUsuario(Usuario usuario);
        Usuario Consultar(Guid id);
    }
}
