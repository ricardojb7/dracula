﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dracula.WebService.Controllers;
using Dracula.Application;
using Dracula.Domain;
using Dracula.Repository;

namespace Dracula.WebService.Controllers
{
    public class UsuarioController : ApiController
    {
        private string conexao = "Server=localhost; Port=27017; Database=dracula;";
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository(conexao);

            if (!usuarioRepository.Exist(id))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Usuario não encontrado");
            }

            Domain.Entidades.Usuario usuario = usuarioRepository.ConsultaUsuario(id);

            return Request.CreateResponse(HttpStatusCode.OK, cliente);
        }

        // POST api/<controller>
        public HttpResponseMessage Post(Dominio.Entidades.Cliente cliente)
        {

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}